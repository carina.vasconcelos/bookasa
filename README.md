Guten Tag liebe Mitarbeiter, 

Im folgenden File erkläre ich euch kurz wie man auf Gitlab "pushen" kann usw.

Das wichtigste für jetzt ist das ihr alle ein SSH Keypair generiert um eine 
sichere Verbindung zu GitLab zu schaffen. 

1.Git Bash öffnen (könnt ihr unter Programe finden)
Danach bitte folgendes mit euren Angaben eingeben:

ssh-keygen -t ed25519 -C "email@example.com"

Falls nach einem Abspeicherungsort gefragt wird, einfach Enter drücken (Default Path).

2. Jetzt noch das eingeben um den Schlüssel zu kopieren:

cat ~/.ssh/id_ed25519.pub | clip

3. Sie gehen jetzt auf ihren Account und dann unter "User Settings" findet ihr "SSH Keys"
Dort könnt ihr dann euren Key reinpasten. 



