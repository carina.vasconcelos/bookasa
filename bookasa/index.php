<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset=utf-8>
	<meta name=viewport content="width=device-width, initial-scale=1">
	<title>Bookasa</title>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/
	3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
	<script src="https://code.jquery.com/jquery-latest.js"></script>
	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
	<script
  src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"
  integrity="sha256-eGE6blurk5sHj+rmkfsGYeKyZx3M4bG+ZlFyA7Kns7E="
  crossorigin="anonymous"></script>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<header id="header" class="header-bookasa">
		<div id="header-inner">
			<a href="index.html"><img id="logo" src="random/logo2..png" alt="Bookasa-Logo"></a>
			<div class="topnav">
			     	<a href="#">User</a>
			     	<a href="#">Orte</a>
			     	<a href="#">Top Hotels</a>
			     	<a href="#">Home</a>
			     	<i class="fas fa-bars icon"></i>
			</div>
		</div>
	</header><!-- End of /header -->
	<div id="front-page-img" class="hero-image">
		<h1 class="front-page-text">Worldwide Travel</h1>
		<p class="front-page-text" style="top: 50%;">Mi Bookasa, es su Bookasa!</p>
		<form action="search.php" method="post" accept-charset="utf-8">
			<!-- Preparation for Search Method -->
			<div class="form-group search">
				<input type="text" name="suchtext" id="suchtext" autocomplete="off" class="form-control" size="50" placeholder="Seach for where to go..." >
				<div id="response"></div>
				<script>
					$(document).ready(function() {
						$('#suchtext').keyup(function() {
							var query = $('#suchtext').val();

							if (query.length >= 1) {
								$.ajax(
									{
										url:'livesearch.php',
										method:'POST',
										data: {
											search:1,
											q: query
										},
										success: function(data) {
											$("#response").html(data);
										},
										dataType: 'text'
									}
								);
							}
							$("#response").css({"background-color": "white",
												"padding-left": "10px",
												"border-radius": "0px 0px 3px 3px",
												"padding-bottom": "2px",
												"padding-top": "2px",
												"font-size": "110%" });
							if(query.length < 1) {
								$("#response").hide();
							} else {
								$("#response").show();
							}
						});
					});
				</script>
				
			</div>
		</form>
	</div>
	<!--End of Form-->
	<!--Sticky Boyos-->
	<div class="sticky-container">
		<ul class="sticky">
			<li>
				<a href="#" class="facebook"><i class="fab fa-facebook-f"></i></a> 
			</li>
			<li>
				<a href="#" class="twitter"><i class="fab fa-twitter"></i></a> 
			</li>
			<li>
				<a href="#" class="google"><i class="fab fa-reddit-alien"></i></a> 
			</li>
		</ul>
	</div>
	<!--About&Costumer section starts here nya-->
	<div id="about" class="container-fluid">
		<div class="row">
			<div class="col-sm-8">
				<h2>About Bookasa</h2><br>
				<h4>We aspire to be a better Company and give you a fully costumized Service that was specialized for your needs and desires.</h4>
				<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>
				<br><button class="mybtn">Learn more</button>
			</div>
			<div id="icon1" class="col-sm-4">
				<i class="fas fa-hotel"></i>
			</div>
		</div>
	</div>
	
	<div id="costumers" class="container-fluid bg-grey">
		<div class="row">
			<div id="icon2" class="col-sm-4">
				<i class="fas fa-globe-americas"></i>
			</div>
			<div class="col-sm-8">
				<h2>Our Costumers</h2><br>
				<h4>We aspire to be a better Company and give you a fully costumized Service that was specialized for your needs and desires.</h4>
				<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>
				<br><button class="mybtn">sign up!</button>
			</div>
		</div>
	</div>
	<!--End of About&Costumer Section-->
	<!--Separator to make stuff fancy ya know-->
	<div class="fancy-boi filter"><h2 class="fancy-text"><span id="discover-text">Discover</span></h2></div>
	<!--Some Beautiful pics Slideshow-->
	<div id="discover" class="container-fluid text-center">
		<h2>Where would you go?</h2><br>
		<h4>There are many hidden Places around the World</h4>
		<div class="row text-center slideanim">
			<div class="col-sm-4">
				<div class="thumbnail">
				<!--need image--->
				<p><strong>Hotel #1</strong></p>
				<p>Oh damn its expensive</p>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="thumbnail">
				<!--need image--->
				<p><strong>Hotel #1</strong></p>
				<p>Oh damn its expensive</p>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="thumbnail">
				<!--need image--->
				<p><strong>Hotel #1</strong></p>
				<p>Oh damn its expensive</p>
				</div>
			</div>
		</div>
		
	</div>
</body>
<script>
function navbar_responsive() {
  var x = document.getElementById("my_nav");
  if (x.className === "topnav") {
    x.className += " responsive";
  } else {
    x.className = "topnav";
  }
}
</script>
</html>