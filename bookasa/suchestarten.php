<!DOCTYPE html>
<html lang="de">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Titel der Seite | Name der Website</title>
    <link rel="stylesheet" type="text/css" href="style.css" media="screen" />
    <script src="https://code.jquery.com/jquery-latest.js"></script>
    <script>
    var minlength = 2;
    var oldbegrifftime = new Date();
    function value_changed(){
      var eingabe = document.getElementById('suchtext').value;
      var nowdate = new Date();
      if(eingabe.length >= minlength){
        if(nowdate - oldbegrifftime >= 1000){
        oldbegrifftime = nowdate;
        $(".searchcontent").load("search_tips.php", {var1:eingabe, var2:minlength});
        //Sechs ergebnisse anzeigen mit *Begriff*, falls es nicht 6 Ergebnisse gibt, nach Orte, dann Begriff fals möglich in einzelne Wörter(sclimmsten Fall Buchstaben) zerteilen und einzeln in der Datenbank suchen, bis insgesamt 6 Ergebnisse angezeigt werden.
      }
      else{
        setTimeout(function() {value_changed("");}, 1000)
      }
      }
      else{
        $(".searchcontent").empty();
      }
  }
    </script>
  </head>
  <body>
    <nav>
    <form action='search.php' methode='get'>
      <input type="text" name="suchtext" id="suchtext" placeholder="Suche..." autocomplete="off" oninput="value_changed()">
      </input>
      <input type="submit" name="search" value="Suchen">
	</form>
  <ul class="searchcontent"></ul>
</nav>
  </body>
</html>
