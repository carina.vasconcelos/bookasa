<?php 
if(isset($_POST['search'])) {
	$response = "No data found!";

	$connection = new mysqli('localhost','root','','db_bookasa');
	$q = $connection->real_escape_string($_POST['q']); //real escape string ist gegen Mysql injections

	$sql = $connection->query("SELECT ortname FROM tbl_orte
	WHERE ortname LIKE '%$q%'");
	if ($sql->num_rows > 0) {
		$response = "<ul>";

		while ($data = $sql->fetch_array()) 
			$response .= "<li>".$data['ortname']."</li>";
		
		$response .="</ul>";
	}
	exit($response);
}
?>